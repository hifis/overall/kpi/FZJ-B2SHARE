# FZJ B2SHARE

plt_b2share.py is a script to visualize the accounting data of B2SHARE. The configurations are specified in config_plt_b2share.json.

## Usage

To call the script, a Python interpreter is needed:

```bash
python3 plt_b2share.py
```

## Configurations

The following options are set in the configuration file:

- b2_share_file : the B2SHARE file containing the data to be visualized
- plot_data : the data to be plotted; this should be a column name in b2_share_file and the corresponding column should also be visualizable
- save_png : a truth value, true or false, whether to save a png file of the plot
- save_pdf : a truth value, true or false, whether to save a pdf file of the plot

