
from fileinput import filename
import json
import os.path
import sys
import pandas as pd
import matplotlib.pyplot as plt
import math

# binary mega unit
Gi = 1024 ** 3

def load_config(config_file_name):
    ret = []
    # config file does exist
    with open(config_filename) as json_file:
        config = json.load(json_file)
    
    if config.get('b2_share_file'):
        ret.append(config.get('b2_share_file'))
    else:
        print("Could not find input file")
        sys.exit()

    if config.get('plot_data'):
        plot_data = config.get('plot_data')
        if isinstance(plot_data, list):
            ret.append(plot_data)
        elif isinstance(plot_data, str):
            ret.append([plot_data])
        else:
            print("Type of group variable incompatible")
            sys.exit()
    else:
        print("No data for ploting has been provided")
        sys.exit(0)
     
    if config.get('save_png'):
        ret.append(True)
    else:
        ret.append(False)
    
    if config.get('save_pdf'):
        ret.append(True)
    else:
        ret.append(False)
    return ret

def round_gb_size_up(gb_size, dp=2):
    shift = 10.0**dp
    return float(math.ceil(gb_size * shift))/shift


def convert_bytes_to_gb(n_bytes, low_value=.00001, dp=2):

    gb_size = n_bytes / float(Gi)
    if gb_size < low_value:
        gb_size = low_value
    if dp is not None:
        # round to second decimal place
        gb_size = round_gb_size_up(gb_size, dp=dp)
    return gb_size 



def plot_one_data(data_column, b2_share_df, save_png, save_pdf, unit_df):
    # clear canvas
    plt.clf()

    # convert Byte column to Gigabyte
    if data_column == "Disk Space":
        b2_share_df[data_column] = b2_share_df[data_column].apply(convert_bytes_to_gb)
        unit_df[data_column][1]= "GB"
      
    b2_share_df.groupby('Community')[data_column].plot(legend=True)
    plt.xticks(rotation=45)
    plt.grid(True)
    plt.ylabel(unit_df[data_column][1])
    plt.legend(loc="upper left")
    file_name = data_column.lower().replace(" ", "_")
    if save_png:
        plt.savefig(file_name+".png")
    if save_pdf:
        plt.savefig(file_name+'.pdf', format='pdf')


if __name__ == "__main__":
    config_filename ="config_plt_b2share.json"
    if os.path.exists(config_filename):
        b2_share_file, plot_data, save_png, save_pdf = load_config("config_plt_b2share.json")
    else:
        print("Could not find config file")
        sys.exit()

    # create dataframe
    b2_share_data = pd.read_csv(b2_share_file, header=0, skiprows=[1,2])
    # needed for labeling in plots
    units_df = pd.read_csv(b2_share_file, header=0, skiprows=lambda x: x >2)
    
    # change type to datetime
    b2_share_data["#date-time"] =pd.to_datetime(b2_share_data['#date-time'])
    b2_share_data = b2_share_data.rename(columns={"#date-time":"time"})
    b2_share_data.set_index('time', inplace=True)


    for to_plot_data in plot_data:
        # check if data exists in file
        if to_plot_data in b2_share_data.columns:
            # first row in csv represents wether the data is meant to be plotted
            if int(units_df[to_plot_data][0]) == 1:
                plot_one_data(to_plot_data, b2_share_data, save_png, save_pdf, units_df)
            else:
                print(f"{to_plot_data} is not specified as plotable")

        else:
            print(f"Could not find {to_plot_data} in csv file")

    print("Script ran successfully")
    
